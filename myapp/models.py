from django.db import models
from django.contrib.auth.models import User

# Create your models here.

class News(models.Model):
    title = models.CharField(max_length=100)
    images = models.ImageField(upload_to='images/', blank=True, null=True)
    description = models.TextField()
    add_date = models.DateField(auto_now_add=True)
    author = models.CharField(max_length=100)

    def __str__(self):
        return self.title

class Model2(models.Model):
    GENDER_CHOICES = (
        ('M', "Male"),
        ('F', 'Female')
    )
    first_name = models.CharField(max_length=25)
    last_name = models.CharField(max_length=25)
    birthday = models.DateField()
    gender = models.CharField(choices=GENDER_CHOICES, max_length=128)

    def __str__(self):
        return f"{self.first_name} {self.last_name}"

class Model3(models.Model):
    title = models.CharField(max_length=100)
    description = models.TextField()
    user = models.OneToOneField(User, on_delete=models.CASCADE, blank=True, null=True)

    def __str__(self):
        return self.title

class Model4(models.Model):
    quetion = models.CharField(max_length=255)
    ansver1 = models.CharField(max_length=255)
    ansver2 = models.CharField(max_length=255)
    ansver3 = models.CharField(max_length=255)
    ansver4 = models.CharField(max_length=255)
    

    def __str__(self):
        return self.quetion

class Model5(models.Model):
    topic = models.CharField(max_length=100)
    data = models.DateField()

    def __str__(self):
        return self.topic