from django.urls import path
from .views import CreateList, ListView, UpdateView, DeleteView, CreateList2, ListView2, UpdateView2, DeleteView2, CreateList3, ListView3, UpdateView3, DeleteView3, CreateList4, ListView4, UpdateView4, DeleteView4, CreateList5, ListView5, UpdateView5, DeleteView5
from django.conf import settings
from django.conf.urls.static import static

urlpatterns = [
    path('', ListView, name='list_view'),
    path('create', CreateList, name='create'),
    path('update/<int:id>', UpdateView, name='update'),
    path('delete/<int:id>', DeleteView, name='delete'),
    path('model2/', ListView2, name='list_view2'),
    path('model2/create2', CreateList2, name='create2'),
    path('model2/update2/<int:id>', UpdateView2, name='update2'),
    path('model2/delete2/<int:id>', DeleteView2, name='delete2'),
    path('model3/', ListView3, name='list_view2'),
    path('model3/create3', CreateList3, name='create3'),
    path('model3/update3/<int:id>', UpdateView3, name='update3'),
    path('model3/delete3/<int:id>', DeleteView3, name='delete3'),
    path('model4/', ListView4, name='list_view4'),
    path('model4/create4', CreateList4, name='create4'),
    path('model4/update4/<int:id>', UpdateView4, name='update4'),
    path('model4/delete4/<int:id>', DeleteView4, name='delete4'),
    path('model5/', ListView5, name='list_view5'),
    path('model5/create5', CreateList5, name='create5'),
    path('model5/update5/<int:id>', UpdateView5, name='update5'),
    path('model5/delete5/<int:id>', DeleteView5, name='delete5'),
]



if settings.DEBUG: 
        urlpatterns += static(settings.MEDIA_URL, 
                              document_root=settings.MEDIA_ROOT) 