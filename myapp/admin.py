from django.contrib import admin
from .models import News, Model2, Model3, Model4, Model5 

# Register your models here.

admin.site.register(News)
admin.site.register(Model2)
admin.site.register(Model3)
admin.site.register(Model4)
admin.site.register(Model5)