from django import forms
from .models import News, Model2, Model3, Model4, Model5

class NewsForm(forms.ModelForm):
    
    class Meta:
        model = News
        fields = '__all__'

class Model2Form(forms.ModelForm):
    
    class Meta:
        model = Model2
        fields = '__all__'

class Model3Form(forms.ModelForm):
    
    class Meta:
        model = Model3
        fields = ['title', 'description']

class Model4Form(forms.ModelForm):
    
    class Meta:
        model = Model4
        fields = '__all__'

class Model5Form(forms.ModelForm):
    
    class Meta:
        model = Model5
        fields = ['topic', 'data']