from django.shortcuts import render, redirect, get_object_or_404
from .models import News, Model2, Model3, Model4, Model5
from .forms import NewsForm, Model2Form, Model3Form, Model4Form, Model5Form

# Create your views here.


def CreateList(request):
    form = NewsForm()
    if request.method == "POST":
        form = NewsForm(request.POST, request.FILES)
        if form.is_valid():
            form.save()
            return redirect('/')
        
    return render(request, "create.html", {'form': form})

def ListView(request):  

    news = News.objects.all()

    return render(request, "list_view.html", {'news': news})

def UpdateView(request, id): 
  
    news = News.objects.get(id=id)
    form = NewsForm(instance=news) 

    if request.method == "POST":
        form = NewsForm(request.POST,request.FILES, instance=news)  
        if form.is_valid(): 
            form.save() 
            return redirect('/')
 
  
    return render(request, "update.html", {'form': form})

def DeleteView(request, id): 
  
    news = News.objects.get(id=id)
    form = NewsForm(instance=news) 

    if request.method == "POST":
        news.delete()
        return redirect('/')
 
  
    return render(request, "delete.html", {'form': form})

# bu model1
def CreateList2(request):
    form = Model2Form()
    if request.method == "POST":
        form = Model2Form(request.POST)
        if form.is_valid():
            form.save()
            return redirect('/model2')
        
    return render(request, "model2/m2_create.html", {'form': form})

def ListView2(request):  

    model2s = Model2.objects.all()

    return render(request, "model2/m2_list_view.html", {'model2s': model2s})

def UpdateView2(request, id): 
  
    model2s = Model2.objects.get(id=id)
    form = Model2Form(instance=model2s) 

    if request.method == "POST":
        form = Model2Form(request.POST, instance=model2s)  
        if form.is_valid(): 
            form.save() 
            return redirect('/model2')
 
  
    return render(request, "model2/m2_update.html", {'form': form})

def DeleteView2(request, id): 
  
    model2s = Model2Form.objects.get(id=id)
    form = Model2Form(instance=model2s) 

    if request.method == "POST":
        model2s.delete()
        return redirect('/model2')
 
  
    return render(request, "model2/m2_delete.html", {'form': form})

#  bu model3

def CreateList3(request):
    form = Model3Form()
    if request.method == "POST":
        form = Model3Form(request.POST)
        if form.is_valid():
            model22 = form.save(commit=False)
            model22.user = request.user
            model22.save()
            return redirect('/model3')
        
    return render(request, "model3/m3_create.html", {'form': form})

def ListView3(request):  

    model3s = Model3.objects.all()

    return render(request, "model3/m3_list_view.html", {'model3s': model3s})

def UpdateView3(request, id): 
  
    model3s = Model3.objects.get(id=id)
    form = Model3Form(instance=model3s) 

    if request.method == "POST":
        form = Model3Form(request.POST, instance=model3s)  
        if form.is_valid(): 
            form.save() 
            return redirect('/model3')
 
  
    return render(request, "model3/m3_update.html", {'form': form})

def DeleteView3(request, id): 
  
    model3s = Model3.objects.get(id=id)
    form = Model3Form(instance=model3s) 

    if request.method == "POST":
        model3s.delete()
        return redirect('/model3')
 
  
    return render(request, "model3/m3_delete.html", {'form': form})

#  bu model4

def CreateList4(request):
    form = Model4Form()
    if request.method == "POST":
        form = Model4Form(request.POST)
        if form.is_valid():
            form.save()            
            return redirect('/model4')
        
    return render(request, "model4/m4_create.html", {'form': form})

def ListView4(request):  

    model4s = Model4.objects.all()

    return render(request, "model4/m4_list_view.html", {'model4s': model4s})

def UpdateView4(request, id): 
  
    model4s = Model4.objects.get(id=id)
    form = Model4Form(instance=model4s) 

    if request.method == "POST":
        form = Model4Form(request.POST, instance=model4s)  
        if form.is_valid(): 
            form.save() 
            return redirect('/model4')
 
  
    return render(request, "model4/m4_update.html", {'form': form})

def DeleteView4(request, id): 
  
    model4s = Model4.objects.get(id=id)
    form = Model4Form(instance=model4s) 

    if request.method == "POST":
        model4s.delete()
        return redirect('/model4')
 
  
    return render(request, "model4/m4_delete.html", {'form': form})

#  bu model5

def CreateList5(request):
    form = Model5Form()
    if request.method == "POST":
        form = Model5Form(request.POST)
        if form.is_valid():
            form.save()            
            return redirect('/model5')
        
    return render(request, "model5/m5_create.html", {'form': form})

def ListView5(request):  

    model5s = Model5.objects.all()

    return render(request, "model5/m5_list_view.html", {'model5s': model5s})

def UpdateView5(request, id): 
  
    model5s = Model5.objects.get(id=id)
    form = Model5Form(instance=model5s) 

    if request.method == "POST":
        form = Model5Form(request.POST, instance=model5s)  
        if form.is_valid(): 
            form.save() 
            return redirect('/model5')
 
  
    return render(request, "model5/m5_update.html", {'form': form})

def DeleteView5(request, id): 
  
    model5s = Model5.objects.get(id=id)
    form = Model5Form(instance=model5s) 

    if request.method == "POST":
        model5s.delete()
        return redirect('/model5')
 
  
    return render(request, "model5/m5_delete.html", {'form': form})